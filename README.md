# Trust 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development 
and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites and setup

First, check if you have Python3. If not, so install it. 
```
python3 -V
```
Install pip to manage software packages for Python:
```
sudo apt-get install -y python3-pip
```
After that run:

```
pip3 install -e trust
```

And then:

```
pip3 install -r requirements.txt
```
Then, in a separate terminal run:
```
export PYTHONPATH=$PYTHONPATH:/path/to/task_classes:/path/to/other_task_classes
ipcluser start
```

Where the "task_classes" folder is where the classes are (for the test examples is test/task_classes. This will allow to execute the tasks in parallel. 

It is neccesary to define enviroment variables that are used at the config files. For example, in the test examples, the environment variable is PIPELINE_SYSTEM_PATH. The yamls files will be processes to replace all the enviroment variable to the defined value. The config file should define the enviroment variable with this structure: `${NAME}`. 

### Using the system

It is necessary to have:
 * config file, which have all the necessary information to design the pipelines
 * script file, which will run the pipelines

## Test examples

In folder test there are two demo examples. One (1) without paralellization of process, and the other (2), with paralellized procees. Following, there are some general instructions.

### Config file interface

The config.yaml should have taks with the following settings. For example, the **model** task, has some input and output files and some parameters
  
 	model:  
	    input:
	      - data/units.pickle
	      - data/split.pickle
	      - features/feature_selected.pickle
	    output:
	      - models/model.pickle

	    parameters:
	      model_type: svm
	      C: [0.01,0.001]
	      kernel: ['linear','linearSVC']
	      std_divide: true
	      max_iter: 5000
	     
	    explore:
	      - C
	      - kernel

### Pipeline instance

After importing the module, the next step is to load a **config.yaml** file and initiate a **Pipeline** instance


```python
from system.pipeline import Pipeline
os.environ['BASE_PATH'] = '/home/miles/trust/test'
pipe = Pipeline('configs/demo1_config.yaml')
pipe.caching = False
```

If the caching property is set to true, then the tasks will check if they where runned before and prevent computing the results. The caching option is set to True by default.

### Running Tasks

To run a task use the **run_task** command. If the task are not runned at the correct order, input and outputs may be missing.


```python
pipe.run_task('data')
pipe.run_task('pca_transform')
pipe.run_task('split')
pipe.run_task('split2')
pipe.run_task('feature_selector')
pipe.run_task('model')
pipe.run_task('metrics')
```

### Loading io files

The pipeline instance has access to all the input and outputs that are used. This is done by using the **iodata** dictionary. For example, in the following, the address models/metrics.pickle is loaded. This has the dataframe returned by the metric task


```python
pipe.iodata['models/metrics.pickle'].load()
```

<div>

<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th>general</th>
      <th colspan="9" halign="left">test</th>
      <th>...</th>
      <th colspan="10" halign="left">train</th>
    </tr>
    <tr>
      <th></th>
      <th>labels</th>
      <th>accuracy</th>
      <th>auc</th>
      <th>cm</th>
      <th>eer</th>
      <th>f1_score_unweighted</th>
      <th>f1_score_weighted</th>
      <th>fpr</th>
      <th>hist</th>
      <th>kdes</th>
      <th>...</th>
      <th>f1_score_weighted</th>
      <th>fpr</th>
      <th>hist</th>
      <th>kdes</th>
      <th>majority</th>
      <th>precision_unweighted</th>
      <th>precision_weighted</th>
      <th>recall_unweighted</th>
      <th>recall_weighted</th>
      <th>tpr</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>[0, 1]</td>
      <td>0.927711</td>
      <td>0.96671</td>
      <td>[[117, 7], [11, 114]]</td>
      <td>0.080323</td>
      <td>0.9277</td>
      <td>0.927697</td>
      <td>[0.0, 0.0, 0.008064516129032258, 0.00806451612...</td>
      <td>[([-1.9496236111398355, -1.6889025636529045, -...</td>
      <td>[([-3.383589372317956, -3.378369731727627, -3....</td>
      <td>...</td>
      <td>0.912334</td>
      <td>[0.0, 0.0, 0.008, 0.008, 0.032, 0.032, 0.04, 0...</td>
      <td>[([-2.139157446554475, -1.8433236003207125, -1...</td>
      <td>[([-3.766243600840169, -3.7603210013159694, -3...</td>
      <td>0.501992</td>
      <td>0.912727</td>
      <td>0.912781</td>
      <td>0.912413</td>
      <td>0.912351</td>
      <td>[0.007936507936507936, 0.5396825396825397, 0.5...</td>
    </tr>
  </tbody>
</table>
<p>1 rows × 31 columns</p>
</div>



### Ploting some results

The metrics task computes the confusion matrix and ROC curve, so is possible to extract those from the returnes dataframe and plot them.


```python
from system.plots import plot_roc, plot_confusion_matrix
df_metric = pipe.iodata['models/metrics.pickle'].load()
plt.figure(figsize=(12,6))
plt.subplot(1,2,1)
plot_roc(df_metric.test.fpr.values[0], df_metric.test.tpr.values[0])
plt.subplot(1,2,2)
plot_confusion_matrix(df_metric.test.cm.values[0], df_metric.general['labels'].values[0])
plt.tight_layout()
```


```python
df_metric.test[['accuracy','auc','recall_unweighted', 'precision_unweighted', 'f1_score_unweighted']].plot.bar()
```

### Change of parameters

To change a parameter on the fly without the config, a dictionary with keys relating to a task and a dictionary relating to the parameters is passed to the **parameters_update** method.


```python
pipe.parameters_update({'model':{'max_iter':3000}})
print('Task class',pipe.tasks['model'])
print(pipe.iodata['models/model.pickle'].address)
```

    Task class Task(name=model, class=model, parameters={'std_divide': True, 'kernel': 'linear', 'model_type': 'svm', 'C': 0.01, 'max_iter': 3000})
    /home/miles/trust/test/demo1/models/data_n_features_3-data_n_informative_2-feature_selector_feature_select_0-model_C_0.01-model_ker_linear-model_max_iter_3000-model_model_type_svm-model_std_divide_True-spl_k_fold_0-spl_n_spls_2/model.pickle


## Explore methods

The system is able to explore many pipelines configuration, serially or in parallel. This is done by reading the parameters in the config that have the **explore** property. To see all the combination in the grid search, the **explore_parameters** method is used.


```python
pipe = Pipeline('configs/demo1_config.yaml')
pipe.explore_parameters()
```

<div>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th>feature_selector</th>
      <th colspan="2" halign="left">model</th>
      <th>split</th>
    </tr>
    <tr>
      <th></th>
      <th>feature_select</th>
      <th>C</th>
      <th>kernel</th>
      <th>k_fold</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>0.010</td>
      <td>linear</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0</td>
      <td>0.010</td>
      <td>linear</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0</td>
      <td>0.010</td>
      <td>linearSVC</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0</td>
      <td>0.010</td>
      <td>linearSVC</td>
      <td>1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0</td>
      <td>0.001</td>
      <td>linear</td>
      <td>0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>0</td>
      <td>0.001</td>
      <td>linear</td>
      <td>1</td>
    </tr>
    <tr>
      <th>6</th>
      <td>0</td>
      <td>0.001</td>
      <td>linearSVC</td>
      <td>0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>0</td>
      <td>0.001</td>
      <td>linearSVC</td>
      <td>1</td>
    </tr>
    <tr>
      <th>8</th>
      <td>1</td>
      <td>0.010</td>
      <td>linear</td>
      <td>0</td>
    </tr>
    <tr>
      <th>9</th>
      <td>1</td>
      <td>0.010</td>
      <td>linear</td>
      <td>1</td>
    </tr>
    <tr>
      <th>10</th>
      <td>1</td>
      <td>0.010</td>
      <td>linearSVC</td>
      <td>0</td>
    </tr>
    <tr>
      <th>11</th>
      <td>1</td>
      <td>0.010</td>
      <td>linearSVC</td>
      <td>1</td>
    </tr>
    <tr>
      <th>12</th>
      <td>1</td>
      <td>0.001</td>
      <td>linear</td>
      <td>0</td>
    </tr>
    <tr>
      <th>13</th>
      <td>1</td>
      <td>0.001</td>
      <td>linear</td>
      <td>1</td>
    </tr>
    <tr>
      <th>14</th>
      <td>1</td>
      <td>0.001</td>
      <td>linearSVC</td>
      <td>0</td>
    </tr>
    <tr>
      <th>15</th>
      <td>1</td>
      <td>0.001</td>
      <td>linearSVC</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>



### Target task

Is possible to see the order in where the tasks need to be run to fullfill the dependencies by asking the queue to a specific target task.


```python
pipe.get_queue('metrics')
```
    ['data', 'pca_transform', 'feature_selector', 'split', 'model', 'metrics']

### Running an exploration

Is possible to run the full grid search assigned, by indicating the target task at where the pipeline need to reach. This is done by **explore_run(target)**


```python
pipe.caching = False
pipe.explore_run('metrics')
```

### Compilation of outputs

To see al the outputs of the exploration in a single dataframe, **compile_dataframes**


```python
df_compiled_metrics = pipe.compile_data('models/metrics.pickle')
# df_compiled_metrics.set_index(list(pipe.explore_parameters().columns),inplace=True)
df_compiled_metrics.test[['accuracy','auc','recall_unweighted', 'precision_unweighted', 'f1_score_unweighted']].plot.bar(figsize=(14,10))
```



<!-- 
    <matplotlib.axes._subplots.AxesSubplot at 0x7fa8908572b0>




![png](output_22_1.png) -->


## Running an exploration in parallel

Some precautions are needed when running a parallel execution of pipelines. Some prior steps are needed, if there are portions of the tasks that are not te be explored and that they wherent runned before. For example, by starting with an empty folder, and running the first two tasks ('data' and 'pca_transform'), the systems has now the possibility to run the subsecuent exploration in parallel.
<!-- 

```python
import shutil
try: 
    shutil.rmtree('demo1')
except: 
    pass

pipe = Pipeline('configs/demo1_config.yaml')
pipe.caching = True

pipe.run_task('data')
pipe.run_task('pca_transform')
pipe.explore_run('metrics', parallel=True,n_jobs=4)
```

```python
df_compiled_metrics = pipe.compile_data('models/metrics.pickle')
df_compiled_metrics.test.accuracy.iloc[0], 0.896414342629482
```
  -->