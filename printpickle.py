#!/usr/bin/python3
from sklearn.externals import joblib
import argparse
from IPython import embed


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--embed", help="Dont print and embed", action='store_true', default=False)
    parser.add_argument("-c", "--columns", help="print list of columns", action='store_true')
    parser.add_argument("-o", "--output", help="columns names to show")
#    parser.add_argument("-o", "--output", help="columns names to show", default=
#                        "[('test', 'recall_unweighted'), ('test', 'recall_weighted'), ('test', 'auc'), ('test', 'eer')]")
    parser.add_argument("-f", "--fmt", help=None)
    parser.add_argument("input", help="input pickle file", metavar="input")

    # read arguments from the command line
    args = parser.parse_args()
    print(args.input)

    pickle_path = args.input
    df = joblib.load(pickle_path)

    if args.columns:
        to_print = list(df.columns)
    elif args.output:
        cols = eval(args.output)
        to_print = df[cols]
    else:
        to_print = df

    if args.fmt == "csv":
        to_print = to_print.to_csv(float_format='%.3f')

    if args.embed:
        embed()
    else:
        print(to_print)


if __name__ == '__main__':
    main()
