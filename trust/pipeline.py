from IPython import embed
import logging
from addict.addict import Dict
from pathlib import Path
from sklearn.externals import joblib
from itertools import product
import pandas as pd
import yaml
import json
import os
import tempfile
import networkx as nx
from networkx import bfs_edges
import copy
from collections import OrderedDict
import time
from .utils import create_complete_path, create_directory_ignoring_file, hashstring
from .utils import max_distance_bfs, traductor, export_config, to_order_dict, remove_duplicates
import pprint
import shutil
import pandas as pd
from joblib import Parallel, delayed
import pickle
from filelock import FileLock


# TODO create logfileconfig ini

logFormatter = logging.Formatter(
    "%(asctime)s [%(processName)-12.12s] [%(process)s] [%(levelname)-5.5s]  %(message)s")

logger = logging.getLogger('pipeline')
logger.setLevel(logging.INFO)
logger.propagate = False

fileHandler = logging.FileHandler('pipeline.log', mode='w')
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.DEBUG)
logger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
logger.addHandler(consoleHandler)

linebreak = '\n\n---------------------------------------------------------------------------------------------------\n'

# This is called by joblib parallel so it needs to be outside a class to be pickeable


def explore_execute(pipe, queue):
    logger.info('Queue to run:' + str(queue))
    for q in queue:
        pipe.run_task(q)


class Pipeline:
    def __init__(self, *args, log=True, simulate=False):

        if not log:
            logging.disable(logging.INFO)

        logger.info('Configurating Pipeline ' + linebreak)

        self.simulate = simulate
        self.cwd = os.getcwd()

        self.caching = True
        if not isinstance(args, list):
            args = [args]
        self.config_files = args[0]

        self.configs = []

        for config_ in self.config_files:
            self.recursive_include(
                '/'.join(config_.split('/')[:-1]) + '/', config_.split('/')[-1])

        self.config = Dict()
        for c in self.configs:
            self.config.update(c)
        self.setup()

        self.propagate_all_cache()

    def propagate_all_cache(self):
        for k, v in self.tasks.items():
            if v.caching is False:
                self.propagate_cache(k)

    def recursive_include(self, path, config_name):
        from .utils import open_config_file
        logger.info('Opening config file {}'.format(config_name))
        config_ = open_config_file(str(Path(path, config_name)))
        if 'include' in config_.keys():
            if type(config_.include) != list:
                config_.include = [config_.include]
            for ci in config_.include:
                self.recursive_include(path, ci)
        self.configs.append(config_)

    def setup(self):
        from pathlib import Path
        import importlib
        import sys

        self.tasks_modules = []
        if 'tasks_module' in self.config.globals.keys():
            tasks_modules = self.config.globals['tasks_module']
            if type(self.config.globals['tasks_module']) != list:
                tasks_modules = [tasks_modules]
            for tasks_module_ in tasks_modules:
                module_path = '/'.join(tasks_module_.split('/'))
                self.tasks_modules.append(module_path)
        else:
            raise Exception('Missing tasks_module in global section')

        self.iodata2tasks = {}
        for k, v in self.config.tasks.items():

            for io in v.input:
                if io not in self.iodata2tasks.keys():
                    self.iodata2tasks[io] = [[k], []]
                else:
                    self.iodata2tasks[io][0].append(k)
            for io in v.output:
                if io not in self.iodata2tasks.keys():
                    self.iodata2tasks[io] = [[], [k]]
                else:
                    self.iodata2tasks[io][1].append(k)

        self.tasks2iodatas = {}
        for k, v in self.config.tasks.items():
            self.tasks2iodatas[k] = [v.input, v.output]

        self.tasks = Dict()
        for task_name, v in self.config.tasks.items():
            if 'class' in v.keys():
                class_name = v['class']
            else:
                class_name = task_name

            class_found = False
            for module_path in self.tasks_modules:
                if Path('{}/{}.py'.format(module_path, class_name)).exists():
                    logger.info('Loading task class {} from module {}'.format(
                        class_name, module_path))
                    sys.path.insert(0, module_path)
                    task_module = getattr(importlib.import_module(
                        class_name), class_name)
                    task = task_module(task_name)
                    if 'cache' in v.keys():
                        task.caching = v['cache']
                    if 'extra' in v.keys():
                        task.extra = v['extra']
                    if 'input_pipeline' in v.keys():
                        task.input_pipeline = True
                    self.tasks[task_name] = task
                    class_found = True
                    # sys.path.remove(module_path)
                    break
            if not class_found:
                raise Exception(
                    'Missing class {} in {} module'.format(class_name, module_path))

        self.iodata = Dict()
        for iodata_name, task_list in self.iodata2tasks.items():
            iodata_ = IOData(iodata_name)
            if iodata_.path in self.config.globals.path.keys() or iodata_.path is 'pipeline':
                path_ = Path(self.config.globals.path[iodata_.path])
                if not path_.is_absolute():
                    iodata_.path = path_.absolute()
                else:
                    iodata_.path = path_
            else:
                raise Exception('Global path missing:', iodata_.path)
            self.iodata[iodata_name] = iodata_

            if len(task_list[1]) > 1:
                raise Exception(
                    'Duplicate tasks outputs: {}'.format(iodata_name))

        for task_name, task_class in self.tasks.items():
            for i in [0, 1]:
                for io in self.tasks2iodatas[task_name][i]:
                    task_class.iodata[i].append(self.iodata[io])

        # if there is some explore, use the first as default
        config_ = {}
        for task_name, v in self.config.tasks.items():
            config_[task_name] = {}
            if 'parameters' in v.keys():
                parameters_ = v['parameters'].copy()
                if 'explore' in v.keys():
                    for explores in v['explore']:
                        parameters_[explores] = parameters_[explores][0]
                config_[task_name] = parameters_
        self.parameters_update(config_)

    def parameters_update(self, config=None):
        for task_name, task_class in self.tasks.items():
            if config:
                if task_name in config.keys():
                    params_dict = config[task_name]
                    self.tasks[task_name].update_parameters(params_dict)

        for task_name, task_class in self.tasks.items():
            parameters_depedent = self.parameters_dependency(task_name)
            for iodata in self.tasks[task_name].iodata[1]:
                iodata.set_parameters(parameters_depedent)

    def parameters_dependency(self, task_name):
        params_dict = OrderedDict()
        deps = sorted(self.dependency_walker(task_name))

        for d in [k for pair in deps for k in pair]:
            if self.tasks[d].parameters:
                params_dict[d] = self.tasks[d].parameters.copy()
        return to_order_dict(params_dict)

    def dependency_walker(self, task_name, deps=None):
        if deps is None:
            deps = []
        for idata in self.tasks[task_name].iodata[0]:
            for task_name_, task_class_ in self.tasks.items():
                for odata in task_class_.iodata[1]:
                    if idata.name == odata.name:
                        deps.append((task_name, task_name_))
                        self.dependency_walker(task_name_, deps)
        return set(deps)

    def run_task(self, task_name, *args, **kwargs):
        if task_name in self.tasks.keys():
            if self.tasks[task_name].caching is not None:
                caching = self.tasks[task_name].caching
            else:
                caching = self.caching
            running = []

            locks = []
            for iodata_class in self.tasks[task_name].iodata[1]:
                create_directory_ignoring_file(iodata_class.address)
                lock = FileLock(iodata_class.address + '.lock')
                lock.acquire()
                locks.append(lock)

            if caching:
                for iodata_class in self.tasks[task_name].iodata[1]:
                    if iodata_class.IOtype == 'tf':
                        file_ = Path(iodata_class.address + '.index')
                    else:
                        file_ = Path(iodata_class.address)
                    if not file_.exists():
                        logger.info('Not found output: ' + iodata_class.address + ' for task ' + task_name)
                        running.append(True)
                    else:
                        logger.info('Caching task: ' + task_name + ', output: ' + iodata_class.address)
                        running.append(False)

            running = any(running) or len(running) == 0
            if not self.simulate:
                if running or not caching:
                    if self.tasks[task_name].input_pipeline:
                        kwargs.update({'pipeline': self})
                    self.tasks[task_name].run(*args, **kwargs)
            else:
                for iodata in self.tasks[task_name].iodata[1]:
                    iodata._export_config()

            for l in locks:
                l.release()
        else:
            raise Exception('Task: {} not in config file'.format(task_name))

    def run_target(self, target):
        queue = self.get_queue(target)
        for q in queue:
            self.run_task(q)

    def explore_parameters(self):
        exploration_params = [(task_name, param, task_config.parameters[param]) for task_name, task_config in self.config.tasks.items()
                              for param in task_config.explore if 'explore' in task_config.keys()]

        # def list2tuple(x):
        # return tuple(x) if isinstance(x, list) else x

        if len(exploration_params) > 0:
            data = list(product(*[e[2] for e in exploration_params]))
            # data = [list(map(list2tuple, x)) for x in data]
            colum_tuples = [(e[0], e[1]) for e in exploration_params]
            return pd.DataFrame(data, columns=pd.MultiIndex.from_tuples(colum_tuples))
        else:
            return pd.DataFrame([])

    def get_queue(self, target):
        deps = self.dependency_walker(target)
        if len(deps) > 0:
            G = self.construct_graph(deps)
            queue = max_distance_bfs(G, target)
        else:
            queue = [target]

        return queue

    def construct_graph(self, deps):
        G = nx.DiGraph()
        for p, v in deps:
            G.add_edge(v, p)
        G = G.reverse()
        return G

    def construct_full_graph(self):
        tups = []
        for k in self.tasks.keys():
            tups.append((len(self.dependency_walker(k)), k))
        deps = self.dependency_walker(sorted(tups)[-1][1])
        return self.construct_graph(deps)

    def print(self, print_mode='svg'):
        G = self.construct_full_graph().reverse()
        if print_mode == 'basic':
            print(linebreak)
            print(list(nx.topological_sort(G)))
            print(linebreak)
        elif print_mode == 'svg':
            f = tempfile.NamedTemporaryFile(delete=False)
            nx.drawing.nx_agraph.write_dot(G, f.name)
            os.system('dot -Tsvg {} -o pipeline.svg'.format(f.name))
            f.close()

    def propagate_cache(self, source):
        G = self.construct_full_graph()
        if source in G:
            forward_dependencies = set(sum(list(bfs_edges(G.reverse(), source)), ()))
            for task in forward_dependencies:
                if self.tasks[task].caching is None or self.tasks[task].caching:
                    self.tasks[task].caching = False

    def explore_pipelines(self):
        if not hasattr(self, 'df_exploration_params'):
            self.df_exploration_params = self.explore_parameters()

        for r in self.df_exploration_params.to_dict(orient='record'):
            pipe = copy.deepcopy(self)
            params_dict = {k[0]: {} for k, v in r.items()}
            for k, v in r.items():
                params_dict[k[0]][k[1]] = v
            pipe.parameters_update(params_dict)
            yield pipe

    def explore_run(self, target, parallel=False, n_jobs=8):
        queue = self.get_queue(target)
        if parallel:
            Parallel(n_jobs=n_jobs)(delayed(explore_execute)(pipe, queue)
                                    for pipe in self.explore_pipelines())
        else:
            for pipe in self.explore_pipelines():
                explore_execute(pipe, queue)

    def compile_data(self, data_name, list_output=True):
        if not hasattr(self, 'df_exploration_params'):
            self.df_exploration_params = self.explore_parameters()

        data_compilation = []
        for pipe in self.explore_pipelines():
            data_compilation.append(pipe.iodata[data_name].load())

        if not list_output:
            if all([type(c) == pd.DataFrame for c in data_compilation]):
                dfs = []
                df_exploration_params = self.df_exploration_params.copy()
                for i, df_ in enumerate(data_compilation):
                    for col in df_exploration_params:
                        params = df_exploration_params[col].iloc[i]
                        if isinstance(params, list) or isinstance(params, tuple):
                            params = str(params)
                        df_[col] = params
                    dfs.append(df_)

                data_compilation = pd.concat(dfs).reset_index()

        return data_compilation

    def export(self, task_name, output_path, output_label):
        output_extension_directory = time.strftime(
            "%Y-%m-%d-%H-%M-%S") + ('-' + output_label if output_label else '')
        export_path = os.path.join(output_path, output_extension_directory)
        create_complete_path(export_path)
        print('Output path is "%s"' % export_path)

        for output in self.tasks[task_name].iodata[1]:
            address = output.address
            export_filename = '{}/{}'.format(export_path, Path(address).stem)
            df = pd.DataFrame(output.load())
            try:
                df.to_csv(export_filename + '.csv', sep='\t', float_format='%.3f')
                html = df.style.format('{:.3}').render()
            except Exception as e:
                df.to_csv(export_filename + '.csv', sep='\t')
                html = df.style.render()

            with open(export_filename + '.html', 'w') as fp:
                fp.write(html)

            shutil.copyfile(address, export_filename + '.pickle')

        # save config
        if 'include' in self.config:
            self.config.pop('include')
        export_config(self.config, export_path + '/config', 'yaml')


class IOData():
    name = None
    IOtype = None
    address = None

    def __init__(self, name, filename=True):
        self.name = name
        if filename:
            self.path, self.filename = list(
                map(str.strip, self.name.split('/')))
            if '.' in self.filename:
                if self.filename[-2:] == 'tf':
                    self.IOtype = 'tf'
                elif self.filename[-6:] == 'pickle':
                    self.IOtype = 'pickle'
                else:
                    self.IOtype = 'file'
            else:
                self.IOtype = 'path'

    def set_parameters(self, parameters):
        self.parameters = parameters

        if parameters:
            param_strings = []
            for k, v in parameters.items():
                param_string_ = k + '#' + '-'.join(map(lambda t: '{}:{}'.format(*t), v.items()))
                param_strings.append(traductor(param_string_))
            param_path = Path(*param_strings)
        else:
            param_path = Path('')

        path_filename = Path(self.path, param_path, self.filename)

        if any([x > 2**8 for x in map(len, path_filename.parts)]) or len(str(path_filename)) > 2**12 or self.IOtype == 'tf':
            path_filename = Path(self.path, hashstring(str(param_path)), self.filename)

        self.address = str(path_filename)

    def load(self):
        if self.IOtype == 'pickle' or self.IOtype == 'tf':
            return self._read_data(self.address)
        elif self.IOtype == 'path':
            return self.address

    def save(self, Y):
        logger.info('Writing output data in: ' + self.address)
        self._write_data(Y, self.address)
        self._export_config()

    def _export_config(self, mode='yaml'):
        path = Path(self.address)
        extension = path.suffix
        if extension is not '':
            directory = '/'.join(path.parts[:-1])
        else:
            directory = str(path)
        export_config(self.parameters, directory + '/config', mode)

    def __repr__(self):
        return 'IOData(name={}, IOtype={}, address={})'.format(self.name, self.IOtype, self.address)

    def _read_data(self, filename):
        extension = Path(filename).suffix
        try:
            if extension == '.pickle':
                out_ = joblib.load(filename)
                return out_
            elif extension == '.tf':
                with open('{}.class.pickle'.format(filename), 'rb') as fp:
                    model = pickle.load(fp)
                model.load(filename)
                return model
            else:
                raise Exception('File extension not handled')
        except Exception as e:
            print('Couldn not read file:', filename, 'with parameters', self.parameters)
            raise e

    def _write_data(self, data, filename):
        extension = Path(filename).suffix
        create_directory_ignoring_file(filename)
        if extension == '.pickle':
            joblib.dump(data, filename)
        elif extension == '.tf':
            data.save(filename)
        else:
            raise Exception('File extension not handled')


class Task():
    def __init__(self, name):
        self.name = name
        self.task_class = name
        self.iodata = [[], []]
        self.parameters = Dict()
        self.explore = []
        self.caching = None
        self.input_pipeline = None
        self.extra = Dict()

    def __repr__(self):
        return 'Task(name={}, task_class={})'.format(self.name, self.task_class)

    def update_parameters(self, parameters):
        self.parameters.update(parameters)

    def set_parameters(self, parameters):
        self.parameters = parameters

    def __repr__(self):
        return 'Task(name={}, class={}, parameters={})'.format(self.name, self.task_class, str(self.parameters))

    def run(self, **kwargs):
        logger.info('Running task: ' + self.name)
        logger.info('Input:' + pprint.pformat(self.iodata[0]))
        logger.info('Output:' + pprint.pformat(self.iodata[1]))
        logger.info('Config:' + pprint.pformat(self.parameters))
        input_data = self.load_input()

        # if self.process.__name__ == 'parallelizer_wrapper':
        # to check if is a parralelized method it requires the row_indexes argument
        if 'caching' in self.process.__code__.co_varnames:
            self.extra['caching'] = self.caching

        if self.process.__code__.co_varnames[1] == 'row_indexes':
            # pandas is involved in the list of rows to process and the reduce concatenation,
            # it could be an option and use list in the generic case
            # now the row list is the index from the input_data[0]
            row_indexes = input_data[0].index
            output_data = self.process(row_indexes, *input_data, **self.parameters, **self.extra, **kwargs)
        else:
            output_data = self.process(*input_data, **self.parameters, **self.extra, **kwargs)

        logger.info('Writing output for: ' + self.name)
        self.save_output(output_data)
        logger.info('Done task: ' + self.name + linebreak)

    def load_input(self):
        X = []
        for iodata_class in self.iodata[0]:
            X.append(iodata_class.load())
        for iodata_class in self.iodata[1]:
            if iodata_class.IOtype == 'path':
                X.append(iodata_class.address)
        return X

    def save_output(self, Y):
        if not type(Y) in [list, tuple]:
            Y = [Y]
        for i, iodata_class in enumerate(self.iodata[1]):
            if iodata_class.IOtype in ['tf', 'pickle']:
                iodata_class.save(Y[i])
