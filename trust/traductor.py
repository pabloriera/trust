from collections import OrderedDict
traductor = OrderedDict({'osconf_filename': 'osfn',
                         'subject_independent': 'subind',
                         'regularization': 'reg',
                         'degree': 'deg',
                         'max_duration': 'mxdur',
                         'min_duration': 'midur',
                         'classes': 'cls',
                         'smoothing': 'smo',
                         'kernel': 'ker',
                         'c_scale': 'csc',
                         'features_list': 'feats',
                         'split': 'spl',
                         'hop': 'hop',
                         'frame_length': 'frmlen',
                         'f0_max': 'f0mx',
                         'f0_min': 'f0mi',
                         'custom_function': 'cusfun',
                         'class_label': 'clla',
                         'arguments': 'args',
                         'stratified_sample_units': 'ssu',
                         'acoustic_features': 'acfe',
                         'opensmile': 'opsm',
                         'label_encoder': 'laen',
                         'std_divide': 'sdiv',
                         'model_type': 'mdlt',
                         'units_filter': 'unfi',
                         'column': 'col',
                         'mean_substract': 'msub'
                         })
