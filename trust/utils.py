from IPython import embed
import json
import os
import shutil
import time
import re
import collections
from pathlib import Path
import yaml
from collections import OrderedDict
import pandas as pd
from addict import Dict

PATTER_PATH = re.compile(r'^\$\{(.*)\}(.*)$')


def df_print_full(x):
    pd.set_option('display.max_rows', len(x))
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', 2000)
    pd.set_option('display.float_format', '{:20,.3f}'.format)
    pd.set_option('display.max_colwidth', -1)
    print(x)
    pd.reset_option('display.max_rows')
    pd.reset_option('display.max_columns')
    pd.reset_option('display.width')
    pd.reset_option('display.float_format')
    pd.reset_option('display.max_colwidth')


def export_config(conf, filename, mode):
    d = clean_dict(conf)
    filename = filename + '.' + mode
    create_directory_ignoring_file(filename)
    if mode == 'json':
        with open(filename, 'w') as fp:
            json.dump(d, fp)
    elif mode == 'yaml':
        with open(filename, 'w') as fp:
            yaml.dump(d, fp, default_flow_style=False)


def clean_dict(d):
    for k, v in d.items():
        if isinstance(v, collections.Mapping):
            # if isinstance(v, Dict):
                # embed()
            d[k] = clean_dict(dict(v))
        elif isinstance(v, list):
            for i, v_ in enumerate(v):
                if isinstance(v_, collections.Mapping):
                    d[k][i] = clean_dict(dict(v_))
        # elif isinstance(v, np.int64):
        #     d[k] = int(v)
        else:
            d[k] = v
    return dict(d)


def to_order_dict(d):
    def ods(d_):
        return OrderedDict(sorted(d_.items()))

    if isinstance(d, collections.Mapping):
        for k, v in d.items():
            if isinstance(v, collections.Mapping):
                d[k] = to_order_dict(ods(v))
        return ods(d)
    elif isinstance(d, list):
        for i, v in enumerate(d):
            if isinstance(v, collections.Mapping):
                d[i] = to_order_dict(ods(v))
    return d

    # if isinstance(v, collections.Mapping):
    #     d[k] = to_order_dict(ods(v))
    # elif isinstance(v, list):
    #     for i, v_ in enumerate(v):
    #         if isinstance(v_, collections.Mapping):
    #             d[k][i] = to_order_dict(ods(v_))
    # else:
    #     d[k] = v
    # return ods(d)


def args_to_dict(func, locals):
    import inspect
    return {k: locals[k] for k in inspect.signature(func).parameters.keys()}


def make_keras_picklable():
    import tempfile
    import keras.models

    def __getstate__(self):
        model_str = ""
        with tempfile.NamedTemporaryFile(suffix='.hdf5', delete=True) as fd:
            keras.models.save_model(self, fd.name, overwrite=True)
            model_str = fd.read()
        d = {'model_str': model_str}
        return d

    def __setstate__(self, state):
        with tempfile.NamedTemporaryFile(suffix='.hdf5', delete=True) as fd:
            fd.write(state['model_str'])
            fd.flush()
            model = keras.models.load_model(fd.name)
        self.__dict__ = model.__dict__

    cls = keras.models.Model
    cls.__getstate__ = __getstate__
    cls.__setstate__ = __setstate__


def create_directory_ignoring_file(path_filename):
    Path(*Path(path_filename).parts[:-1]).mkdir(parents=True, exist_ok=True)


def create_complete_path(path):
    Path(path).mkdir(parents=True, exist_ok=True)


def read_pickle(filename):
    from sklearn.externals import joblib
    return joblib.load(filename)


def max_distance_bfs(G, source):
    import operator

    node_steps = []
    por_ver = [source]
    step = 0
    while len(por_ver) > 0:
        step += 1
        for n in por_ver:
            node_steps.append((n, step))
        por_ver = sum([list(G[n]) for n in por_ver], [])

    d_node_steps = dict(node_steps)

    for m, p in node_steps:
        d_node_steps[m] = max(d_node_steps[m], p)

    d_node_steps = sorted(d_node_steps.items(), key=operator.itemgetter(1), reverse=True)

    return [n for n, s in d_node_steps]


def remove_duplicates(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


def hashstring(str_):
    import hashlib
    return hashlib.md5(str_.encode()).hexdigest()


def hash_path_filename(path_filename):
    from pathlib import Path
    import numpy as np
    pa = Path(path_filename)
    if len(path_filename) < 200:
        # try:
        # Path(path_filename).exists()
        return path_filename
    # except:
    else:
        parts = list(pa.parts)
        ix = np.argmax(list(map(len, pa.parts)))
        parts[ix] = hashstring(parts[ix])
        path_filename_hash = '/'.join(parts)
        return path_filename_hash


def open_config_file(filename):
    with open(filename, 'r') as stream:
        try:
            yaml.add_implicit_resolver("!pathex", PATTER_PATH)
            yaml.add_constructor("!pathex", replace_env_variables)
            return Dict(OrderedDict(yaml.load(stream)))
        except yaml.YAMLError as exc:
            print(exc)


def replace_env_variables(loader, node):
    value = loader.construct_scalar(node)
    envVar, remain = PATTER_PATH.match(value).groups()
    try:
        return os.environ[envVar] + remain
    except KeyError:
        print("You don't have defined the enviroment variable %s" % envVar)
        raise


def parallelizer(func):
    try:
        import ipyparallel as ipp
        c = ipp.Client(timeout=1.0)
        dv = c[:]
        func.__module__ = ''

        def parallelizer_wrapper(indexes, *args, **kwargs):
            def lfunc(_indexes):
                return func(_indexes, *args, **kwargs)

            print('Parallelizer executing:', func.__name__)
            return ipp.ParallelFunction(dv, lfunc, dist='b', block=None, ordered=True)(indexes).result()

        return parallelizer_wrapper
    except:
        return func


# def whoami():
    # import sys
    # return sys._getframe(1).f_code.co_name


# def read_data(filename, path='', **kwargs):
#     from pathlib import Path
#     extension = Path(filename).suffix
#     path_filename = '{}/{}'.format(path, filename)
#     if extension == '.tsv':
#         import pandas as pd
#         return pd.read_csv(path_filename, sep='\t', **kwargs)
#     elif extension == '.pickle':
#         from sklearn.externals import joblib
#         return joblib.load(path_filename)


# def write_data(data, filename, path='', **kwargs):
#     from pathlib import Path
#     extension = Path(filename).suffix
#     path_filename = '{}/{}'.format(path, filename)
#     create_directory_ignoring_file(path_filename)
#     if extension == '.pickle':
#         from sklearn.externals import joblib
#         joblib.dump(data, path_filename)
#     elif extension == '.tsv':
#         data.to_csv(path_filename, sep='\t', **kwargs)


def write_result_data(script_path, config_path, metrics_files, output_results, output_label):
    output_extension_directory = time.strftime("%Y-%m-%d-%H-%M-%S") + ('-' + output_label if output_label else '')
    result_path = os.path.join(output_results, output_extension_directory)
    create_complete_path(result_path)
    print('Output path is "%s"' % result_path)

    # save config
    shutil.copyfile(config_path, os.path.join(result_path, os.path.basename(config_path)))

    # save list of directories of metrics
    with open(os.path.join(result_path, 'metrics_paths.json'), 'w') as outfile:
        json.dump(metrics_files, outfile)

    # save script
    shutil.copyfile(script_path, os.path.join(result_path, os.path.basename(script_path)))


def traductor(str_):
    from .traductor import traductor

    for k, v in traductor.items():
        str_ = str_.replace(k, v)
    return str_


# def compile_metrics(path):
#     from sklearn.externals import joblib
#     import json
#     import pandas as pd

#     with open(path + '/metrics_paths.json', 'r') as fp:
#         metrics = json.load(fp)

#     df_metrics = []
#     for (comb, path) in metrics:
#         df_metrics.append(joblib.load(path))

#     return pd.concat(df_metrics)


# def input_arguments():
#     import argparse
#     import sys

#     parser = argparse.ArgumentParser()
#     parser.add_argument("-i", "--ifile", help="Input config file.")
#     parser.add_argument("-o", "--opath", help="Output path to save all the results.")
#     parser.add_argument("-r", "--running", help="If it is True, the cache files will be ignored.", action='store_false',
#                         default=True)
#     parser.add_argument("-l", "--label", help="Label that will be add to the output directories.", default="")
#     args = parser.parse_args()

#     if not args.ifile or not args.opath:
#         parser.print_help()
#         sys.exit(2)

#     return args
